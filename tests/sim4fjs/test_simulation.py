import sim4fjs.jobshop
import sim4fjs.instance
import sim4fjs.simulation

def test_machine_simulation_works_with_empty_config():
    jobshop_lines = []
    jobshop_lines.append('1 1 1')
    jobshop_lines.append('1 1 1 3')

    jobshop_instance = sim4fjs.jobshop.Instance(jobshop_lines)
    configuration = { }

    instance = sim4fjs.instance.Instance(configuration, jobshop_instance)
    simulation = sim4fjs.simulation.Simulation(instance)
    run = simulation.run()

    assert run.csv_representation() == [';;0;3', 'Op 0 0;[(1, 3)];1;1']
