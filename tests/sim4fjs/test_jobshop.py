import sim4fjs.jobshop
import tests.utils

def test_jobshop_instance():
    instance = basic_instance()
    verify_jobs(instance)
    verify_operations(instance)
    verify_modes(instance)
        
        
def verify_jobs(instance):
    assert instance.number_of_jobs == 2
    jobs = instance.jobs
    assert len(jobs) == 2


def verify_operations(instance):
    jobs = instance.jobs
    assert len(jobs[0].operations) == 1
    assert len(jobs[1].operations) == 2


def verify_modes(instance: sim4fjs.jobshop.Instance):
    assert instance.number_of_machines == 3
    jobs = instance.jobs

    assert jobs[0].operations[0].machine_id(0) == 1
    assert jobs[0].operations[0].duration(0) == 2

    assert jobs[1].operations[0].machine_id(0) == 1
    assert jobs[1].operations[0].duration(0) == 3
    assert jobs[1].operations[0].machine_id(1) == 2
    assert jobs[1].operations[0].duration(1) == 5

    assert jobs[1].operations[1].machine_id(0) == 3
    assert jobs[1].operations[1].duration(0) == 4


def basic_instance():
    return sim4fjs.jobshop.Instance.from_file(
        tests.utils.path_in_instance_directory("simple.txt")
    )
