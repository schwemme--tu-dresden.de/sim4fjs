import sim4fjs.jobshop
import sim4fjs.instance
from sim4fjs.utils import get_global_random_seed
from tests.sim4fjs import test_jobshop

import tests.utils


def test_error_on_non_multiples_of_min_in_stu():
    jobshop_instance = sim4fjs.jobshop.Instance(['0 0'])
    configuration = dict()
    configuration['minutes_per_simulation_time_unit'] = 30
    configuration['test_parameter_in_minutes'] = 15
    try:
        instance = sim4fjs.instance.Instance(configuration, jobshop_instance)
        instance.get_value('test_parameter')
    except ValueError as e:
        error = str(e)
        print(error)
        assert 'test_parameter' in error
        return
    assert False


def test_error_on_inconsistent_parameters():
    jobshop_instance = sim4fjs.jobshop.Instance(['0 0'])
    configuration = dict()
    configuration['minutes_per_simulation_time_unit'] = 30
    configuration['test_parameter'] = 1
    configuration['test_parameter_in_minutes'] = 30

    try:
        sim4fjs.instance.Instance(configuration, jobshop_instance)
    except ValueError as e:
        error = str(e)
        print(error)
        assert 'test_parameter' in error
        return    
    assert False


def test_error_on_inconsistent_min_per_stu():
    jobshop_instance = sim4fjs.jobshop.Instance(['0 0'])
    configuration = dict()
    configuration['minutes_per_simulation_time_unit'] = 25

    try:
        sim4fjs.instance.Instance(configuration, jobshop_instance)
    except ValueError as e:
        error = str(e)
        print(error)
        assert 'minutes_per_simulation_time_unit' in error
        return    
    assert False


def test_simple():
    """Test that job and operation data are correctly read from a basic
    jobshop input file.
    
    """

    instance = __test_instance("simple.yaml")

    test_jobshop.verify_jobs(instance)
    test_jobshop.verify_operations(instance)

    assert instance.number_of_machines == 3
    jobs = instance.jobs

    assert jobs[0].operations[0].alternative_actions[0].machine_id == 1
    assert jobs[0].operations[0].alternative_actions[0].duration == 2

    assert jobs[1].operations[0].alternative_actions[0].machine_id == 1
    assert jobs[1].operations[0].alternative_actions[0].duration == 3

    assert jobs[1].operations[0].alternative_actions[1].machine_id == 2
    assert jobs[1].operations[0].alternative_actions[1].duration == 5
    
    assert jobs[1].operations[1].alternative_actions[0].machine_id == 3
    assert jobs[1].operations[1].alternative_actions[0].duration == 4


def test_random_seed():
    """Test that the random seed is correctly read from yaml."""
    instance = __test_instance("simple.yaml")
    instance.reset_random_seed()
    assert get_global_random_seed() == 1004


def test_min_duration():
    """Test that the job min duration (and by extension the operation
    min duration) is correctly calculated for our test jobshop file."""

    instance = __test_instance("simple.yaml")

    assert instance.jobs[0].min_duration == 2
    assert instance.jobs[1].min_duration == 7


def test_job_start_due_defaults():
    """Test that the correct default values for job start and due are
    used when no configuration file is specified.
    
    This should cover the case where the concurrency and duedate
    parameters are missing in an existing configuration file.

    """

    instance = __test_instance("simple.yaml")

    assert instance.job_concurrency == 0
    assert instance.job_duedate_factor == 1

    for job in instance.jobs:
        assert job.start == 0
        assert job.due == job.min_duration


def test_job_start_due():
    instance = __test_instance("job_start_due.yaml")

    # the first job should always start at 0
    assert instance.jobs[0].start == 0

    # the first job should be due at 3 = 0 + ceil(2 * 1.5)
    # since it starts at 0, its minimum duration is 2,
    # and our factor is 1.5
    assert instance.jobs[0].due == 3

    # the second job should start at 1 = ceil(2 * 0.4)
    # since the first takes 2 time units, the factor is 0.4,
    # and we round up
    assert instance.jobs[1].start == 1

    # the second job should be due at 1 + ceil(7 * 1.5) = 12
    assert instance.jobs[1].due == 12


def test_three_jobs_start_due():
    instance = __test_instance("three_jobs_start_due.yaml")

    # job concurrency: 0.4
    # job due date factor: 1.5

    assert instance.jobs[0].min_duration == 2   # from jobshop file
    assert instance.jobs[0].start == 0          # it's the first job
    assert instance.jobs[0].due == 3            # == ceil(2 * 1.5)

    assert instance.jobs[1].min_duration == 3   # from jobshop file
    assert instance.jobs[1].start == 1          # == ceil(2 * 0.4)
    assert instance.jobs[1].due == 6            # == 1 + ceil(3 * 1.5)

    assert instance.jobs[2].min_duration == 5   # from jobshop file
    assert instance.jobs[2].start == 3          # == 1 + ceil(3 * 0.4)
    assert instance.jobs[2].due == 11           # == 3 + ceil(5 * 1.5)


def __test_instance(config_file: str):
    """A sim4fjs test instance based on the specified config yaml file
    """
    
    return sim4fjs.instance.Instance.from_file(
        tests.utils.path_in_instance_directory(config_file)
    )
