"""Some utility methods only used in tests."""

import os
import tempfile

def path_in_instance_directory(filename: str):
    """Returns the file path to the specified file name
    
    The assumption is that the file is located in the 'instance'
    sub-directory of the directory where the test file that is being
    executed is located.
    """

    path_to_test_file =  os.path.dirname(os.getenv('PYTEST_CURRENT_TEST'))
    if path_to_test_file != "":
        return path_to_test_file + "/instances/" + filename
    else:
        return "instances/" + filename


def parse_csv_line(line: str):
    return [cell_text.strip() for cell_text in line.strip().split(';')]


def assert_csv_lines_are_equal(
    actual_lines: 'list[str]',
    expected_lines: 'list[str]'):

    assert len(actual_lines) == len(expected_lines)

    for i in range(len(actual_lines)):
        actual = parse_csv_line(actual_lines[i])
        expected = parse_csv_line(expected_lines[i])
        assert actual == expected


class TemporaryOutputFile:
    """A handle to a new, empty, and closed temporary file which is
    automatically removed when leaving the ``with`` context.

    This class implements a context manager, see
    https://book.pythontips.com/en/latest/context_managers.html

    """
 
    def __init__(self):
        self.f = tempfile.NamedTemporaryFile(delete=False)
        self.f.close()

    def __enter__(self):
        return self.f
    
    def __exit__(self, type, value, traceback):
        os.unlink(self.f.name)


def assert_are_equal(text: str, filename: str):
    lines = text.split('\n')

    with open(filename, 'r') as file:
        for i, line in enumerate(file.readlines()):
            line = line[:-1] # remove trailing newline

            if lines[i] != line:
                print("Lines differ")
                print(f"Text ({len(lines[i])} characters): |{lines[i]}|")
                print(f"File ({len(line)} characters): |{line}|")
                assert False
