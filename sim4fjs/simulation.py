"""Classes for performing a simulation of a flexible jobshop instance"""

import logging

from collections import defaultdict

# Note that using SimPy results in a warning
# about a deprecated module "imp"
import simpy

from sim4fjs.instance import Instance, MachineAction, Job, Operation
from sim4fjs.utils import indexes


def select_highest_priority_action(actions: 'list[MachineAction]') -> int:
    """Selects the action with the highest priority

    Currently, the machine action with the shortest duration has the
    highest priority.
    
    Change this method to introduce other strategies of choosing one
    of several available machine actions for an operation.
    """

    return min(actions, key = lambda action: action.duration)


class Simulation(object):
    """A simulation determines how an instance would play out in reality

    A simulation does the following:
    - Choose a mode (a machine) for each operation.
    - Keep track of machine usage (only one operation at a time)
    """
    
    jobs: 'list[Job]'

    def __init__(
        self,
        instance: Instance
        ):
        """Creates a simulation that can later be run."""

        self.jobs = instance.jobs
        self._env = simpy.Environment()
       
        self.__all_machines = [
            simpy.Resource(self._env)
            for _ in range(instance.number_of_machines)
        ]
 

    def __get_machine(self, machine_id: int) -> simpy.Resource:
        """Gets the SimPy resource with the specified machine id"""

        # -1 since the first machine has id 1,
        # but the list is zero-based
        return self.__all_machines[machine_id - 1]


    def __get_machines(
        self,
        actions: 'list[MachineAction]'
        ) -> 'list[simpy.Resource]':
        """Gets the SimPy resources for the specified actions. """

        return [self.__get_machine(action.machine_id) for action in actions]
    

    def run(self) -> 'SimulationRun':
        """Run the simulation."""

        self.sim_run = self._create_simulation_run()
        
        self._register_processes()
        
        self._env.run()

        # sanity check: make sure all operations ran
        number_of_operations = sum(len(job.operations) for job in self.jobs)
        number_of_operation_runs = len(self.sim_run.operation_runs)
        assert number_of_operations == number_of_operation_runs, (
            f"Error in simulation: only {number_of_operation_runs} "
            f"out of {number_of_operations} operations ran."
        )

        return self.sim_run


    def _create_simulation_run(self):
        """Create the simulation run for logging events while simulating

        Override this method and provide your own simulation run
        inheriting from SimulationRun, if you want a simulation with
        additional features.
        """

        return SimulationRun(self.jobs)


    def _register_processes(self):
        """Registers the methods doing the simulation with SimPy
        
        Override this method (calling super...())
        to add other processes.
        """

        for job in self.jobs:
            # we simulate each job independently and delegate the
            # simluation to another method
            self._env.process(self.__simulate_job(job))
       

    def __simulate_job(self, job: Job) -> None:
        """Simulates a sim4fjs job with its operations"""

        delay = job.start
        if delay > 0:
            yield self._env.timeout(delay)

        self._env.process(self.__simulate_operation(job.operations[0]))


    def __simulate_operation(self, operation: Operation) -> None:
        """Simulate the operation including the machine action"""
        
        actions = operation.alternative_actions

        # the alternative SimPy resources the operation's actions want
        resources = self.__get_machines(actions)

        # the SimPy requests for all alternative resources
        requests = [resource.request() for resource in resources]

        # wait until at least one request can be granted
        granted_requests = yield simpy.events.AnyOf(self._env, requests)

        # AnyOf keeps the order of requests
        granted_requests = list(granted_requests)
        logging.debug("granted machine requests: %s", granted_requests)
        
        # the following indexes refer to 3 lists: actions, resources, requests
        granted_indexes, refused_indexes = indexes(requests, granted_requests)
        
        granted_actions = [actions[index] for index in granted_indexes]

        prio_action = select_highest_priority_action(granted_actions)
        
        selected_index = actions.index(prio_action)
        selected_request = requests[selected_index]
        
        # release other granted requests
        for granted_index in granted_indexes:
            granted_request = requests[granted_index]
            if (granted_request != selected_request):
                resources[granted_index].release(granted_request)

        # cancel all non-granted requests
        # here it might pay off performance-wise that we use indexes:
        # we do not have to create a set of granted requests to check for
        # each request if it was granted
        for refused_index in refused_indexes:
            refused_request = requests[refused_index]
            resource = refused_request.resource
            if refused_request in resource.users:
                # it is possible that a request was granted but not
                # returned by "AnyOf", so we must release it instead of just
                # cancelling it
                resource.release(refused_request)
            else:
                refused_request.cancel()

        # perform the action, i.e. wait for the correct duration
        yield from self.__simulate_machine_action(prio_action)

        # release our resource
        resources[selected_index].release(selected_request)
        
        # if we are not the last operation of our job - start the next one
        next_operation = operation.next()
        if next_operation:
            self._env.process(self.__simulate_operation(next_operation))


    def __simulate_machine_action(self, action: MachineAction):
        """Simulate a machine action including its sub-actions

        (There need not be any sub-actions.)
        """

        operation = action.operation
        machine_id = action.machine_id

        self.sim_run.log_start(action, self._env.now)

        if len(action.sub_actions) == 0:
            yield self._env.timeout(action.duration)
        else:
            yield from self._simulate_sub_actions(action)

        self.sim_run.log_stop(operation, machine_id, self._env.now)


    def _simulate_sub_actions(self, action: MachineAction):
        """Override to insert sub action simulation."""
        pass

    
class OperationRun:
    """Represents all data for a simluation run of an operation
    """

    machine_id: int
    machine_start: int
    machine_stop: int
    machine_planned_duration: int


    def __init__(self):
        self.machine_id = None
        self.machine_start = None
        self.machine_stop = None
        self.machine_planned_duration = None
       

    def __repr__(self):
        return (
            f"machine_id: {self.machine_id}\n"
            f"machine_start: {self.machine_start}\n"
            f"machine_stop: {self.machine_stop}\n"
            f"machine_planned_duration: {self.machine_planned_duration}\n"
        )


class OperationStateSnapshot:
    """Represents the state of an operation at a point in time."""

    machine_id: int

    def __init__(self):
        pass

    def __repr__(self):
        return str(self.machine_id)


class SimulationRun:
    """Contains information about a completed simulation run
    """

    # read-only
    operations: 'list[Operation]'
    
    def __init__(self,
                 jobs: 'list[Job]'):
                 
        # store the operations to have them in order when creating the
        # csv result representation
        self.operations = []
        for job in jobs:
            for operation in job.operations:
                self.operations.append(operation)

        # map from event time stamps to dicts of operations to snapshots
        self.event_data = defaultdict(dict)

        # ordered list of all event time stamps
        # to efficiently get a range of all event time stamps between
        # two event time stamps
        # An event time stamp is a point in time at which something
        # interesting happens, e.g. a machine starts working.
        self.event_time_stamps = []

        # map from an event time stamp to the position in the event
        # time stamp list to efficiently get a range of all event time
        # stamps between two event time stamps
        self.ets_indices = dict()

        # map from operations to operation runs -
        # one run for each operation
        self.operation_runs = self._initialize_operation_runs()
        "A dictionary containing an operation run for each operation."

        self.__last_event_time = None


    def _initialize_operation_runs(self):
        return defaultdict(OperationRun)


    def _add_event_time_stamp(self, time):
        """Add a new event time stamp
        
        Whenever something interesting happens at a specific time,
        an event time stamp is created.
        """

        if not time in self.ets_indices:
            self.event_time_stamps.append(time)
            self.ets_indices[time] = (
                len(self.event_time_stamps) - 1
            )


    def _event_time_stamp_range(self, start, end):
        return self.event_time_stamps[
            self.ets_indices[start]:self.ets_indices[end] + 1
        ]


    def log_start(self, action: MachineAction, time):
        """Log the start time of a machine action."""
        
        self._add_event_time_stamp(time)
        operation_run = self.operation_runs[action.operation]
        operation_run.machine_id = action.machine_id
        operation_run.machine_start = time
        operation_run.machine_planned_duration = action.duration


    def log_stop(self, operation: Operation, machine_id, time):
        """Log the stop time of a machine action."""

        self._add_event_time_stamp(time)
        operation_run = self.operation_runs[operation]
        operation_run.machine_stop = time

        # now set the snapshots of what happened in this operation run
        # because it is finished now
        start = operation_run.machine_start
        stop = operation_run.machine_stop
        for event_time_stamp in self._event_time_stamp_range(start, stop):
            snapshot = self._create_operation_state_snapshot()
            snapshot.machine_id = machine_id
            self.event_data[event_time_stamp][operation] = snapshot


    def _create_operation_state_snapshot(self):
        """Override to create an particular operation state snapshot.
        """

        return OperationStateSnapshot()


    def csv_representation(self):
        """Create a csv representation of the simulation run."""

        lines = []
        
        # the header line contains our event time stamps,
        # it starts with two ; to account for the two columns
        # of the operation
        header = ";"
        for event_time_stamp in self.event_data:
            header += ";"
            header += str(event_time_stamp)
        lines.append(header)
        
        for operation in self.operations:
            line = "{}".format(operation)
            for event_time_stamp in self.event_data:
                if operation in self.event_data[event_time_stamp]:
                    text = repr(self.event_data[event_time_stamp][operation])
                else:
                    text = ''
                line += ";{}".format(text)
            lines.append(line)
            
        return lines


    @property
    def last_event_time(self):
        """The time of the last event time stamp of the simulation"""

        if self.__last_event_time is None:
            self.__last_event_time = list(self.event_data.items())[-1][0]
        return self.__last_event_time

