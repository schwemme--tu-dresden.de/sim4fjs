"""Classes for representing data from a flexible jobshop .fsj file"""

# https://docs.python.org/3/howto/logging.html
import logging
from sim4fjs.utils import indexes
from typing import Tuple


class Job(object):
    """A job with operations and modes from a jobshop file."""

    id: int
    number_of_operations: int
    operations: 'list[Operation]'


    def __init__(self, id: int, line_integers: 'list[int]'):
        """Parses the integers in a jobshop line to add jobs,

        operations, and modes.
        """

        self.id = id
        self.__line_integers = line_integers
        position = 0
        self.number_of_operations = self.__line_integers[position]
        position += 1
        self.operations = []
        for operation_id in range(self.number_of_operations):
             position = self.__add_operation(operation_id, position)


    def __add_operation(self, operation_id: int, position: int) -> None:
        number_of_modes = self.__line_integers[position]
        position += 1
        modes=[]
        
        for i in range(number_of_modes):
            machine_id = self.__line_integers[position]
            position += 1
            duration = self.__line_integers[position]
            position += 1
            modes.append((machine_id, duration))
            
        operation = Operation(self, operation_id, modes)
        self.operations.append(operation)
        return position
    

    def __repr__(self) -> str:
        return "Job {0} {1}".format(self.id, self.operations)


class Operation(object):
    """A jobshop operation with its (machine) modes."""

    # read only
    job: Job
    id: int
    number_of_modes: int


    def __init__(self, job: Job,
                 operation_id: int,
                 modes: 'list[Tuple[int, int]]'):
        self.job = job
        self.id = operation_id
        self.number_of_modes = len(modes)
        self.__modes = modes
   
        
    def machine_id(self, mode_index: int) -> int:
        try: 
            return self.__modes[mode_index][0]       
        except IndexError:
            self.__raise_index_error(mode_index)
    

    def duration(self, mode_index: int) -> int:
        try: 
            return self.__modes[mode_index][1]       
        except IndexError:
            self.__raise_index_error(mode_index)


    def __raise_index_error(self, mode_index: int) -> None:
        raise IndexError(
            f"Operation {self} has no mode index {mode_index}"
        )


    def __repr__(self) -> str:
        return "Op {0} {1};{2}".format(self.job.id, self.id, self.__modes) 

   
class Instance(object):
    """Contains all information about a standard job shop instance file
    """

    jobs: 'list[Job]'
    number_of_jobs: int
    number_of_machines: int

    def __init__(self, jobshop_lines: 'list[str]'):
        self.jobs = [] 

        # the first line contains the number of jobs and machines
        split_first_line = jobshop_lines[0].split()
        self.number_of_jobs = int(split_first_line[0])
        self.number_of_machines = int(split_first_line[1])
        
        # each remaing line represents a job
        job_id = 0
        for line in jobshop_lines[1:]:
            # leaving out empty lines (e.g. end the end)
            if not line.isspace():
                self.__add_job(job_id, line)
                job_id += 1

        logging.debug("Jobshop instance:\n%s", self)
    

    @classmethod
    def from_file(cls, jobshop_file: str):
        """Create a jobshop instance from a jobshop file
        """
        
        logging.info("reading file %s", jobshop_file)
        with open(jobshop_file) as f:
            jobshop_lines = f.readlines()

        logging.debug("file content:\n%s", jobshop_lines)
        return cls(jobshop_lines)


    def __add_job(self, id: int, line: str) -> None:
        line_integers = [int(number) for number in line.strip().split()]
        job = Job(id, line_integers)
        self.jobs.append(job)
        
        
    def __repr__(self) -> str:
        result = "Instance:"
        for job in self.jobs:
            result += "\n{0}".format(job)
        return result
        