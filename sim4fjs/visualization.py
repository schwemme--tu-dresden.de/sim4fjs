"""Functionality for visualizing instances and simulation results"""

import os

import matplotlib.ticker
import matplotlib.pyplot as plt

from sim4fjs.simulation import SimulationRun
import sim4fjs.instance


automated_color = '#bfbfbf' # light grey

plt.rcParams['figure.figsize'] = [12, 8]


class TimeAxisDescriptor:
    """A class describing properties of our time axis in various charts
    
    points_per_hour are simulation time units per hour
    """

    xlim: int
    points_per_hour: int
    xlabel: str

    def __init__(self, xlim: int, instance: sim4fjs.instance.Instance = None):
        
        self.xlim = xlim
        self.__xlabel = None

        # Try to determine points per hour from the instance.
        # If this is not possible, we use None and do not have a real
        # time axis.
        minutes_per_simulation_time_unit = 0
        if instance is not None:
            minutes_per_simulation_time_unit = instance.get_value(
                'minutes_per_simulation_time_unit', 0
            )
        if minutes_per_simulation_time_unit > 0:
            self.points_per_hour = 60 / minutes_per_simulation_time_unit
        else:
            self.points_per_hour = None


    @property
    def xlabel(self) -> str:
        if self.__xlabel is None:
            if self.points_per_hour is not None:
                return "Real Time [h]"
            else:
                return "Simulation Time Units"
        else:
            self.__xlabel


    @xlabel.setter
    def xlabel(self, value: str):
        """Sets the label of the x (time) axis
        
        Sometimes we do not want a label and we can set it to the
        empty string here.
        """
        self.__xlabel = value


def try_set_real_time_x_ticks(
    ax: plt.Axes,
    time_axis_descriptor: TimeAxisDescriptor) -> None:

    """Try to set ticks for the x (time) axis with useful spacing
    
    Ticks are the places on an axis that are specificially marked
    by a small line and an optional number.

    If our time axis has information about real time, i.e. points
    per hour, this method tries to define a sensible number of xticks
    so a day period can be recognized by looking at the axis.
    Adjusts MAXTICKS if necessary to avoid errors.

    If the time axis has no such information,
    just use the points as they are.
    """

    # The following constants are determined empirically: what is the
    # best distance between numbers on the real time x axis.
    # It might not work for all cases.
    points_per_hour = time_axis_descriptor.points_per_hour
    if points_per_hour is not None:
        if time_axis_descriptor.xlim > 4000 / points_per_hour:
            hours_per_tick = 24
        elif time_axis_descriptor.xlim > 1000 / points_per_hour:
            hours_per_tick = 12
        elif time_axis_descriptor.xlim > 300 / points_per_hour:
            hours_per_tick = 6
        else:
            hours_per_tick = 3

        # hours_per_tick = 24 --> points_per_major_tick = 48
        # with 0.5h per point
        points_per_major_tick = hours_per_tick * 2
        locator = matplotlib.ticker.MultipleLocator(points_per_major_tick)
        locator.MAXTICKS = (
            time_axis_descriptor.xlim / points_per_major_tick
        ) * 2 # to be safe

        ax.xaxis.set_major_locator(locator)
        ticks = matplotlib.ticker.FuncFormatter(
            lambda x, pos: '{0:g}'.format(int(x / points_per_hour) % 24)
        )
        ax.xaxis.set_major_formatter(ticks)

        # hours_per_tick = 24 --> points_per_minor_tick = 8
        # with 0.5h per point
        points_per_minor_tick = hours_per_tick / 3
        locator = matplotlib.ticker.MultipleLocator(points_per_minor_tick)
        locator.MAXTICKS = (
            time_axis_descriptor.xlim / points_per_minor_tick
            ) * 2 # to be safe

        ax.xaxis.set_minor_locator(locator)
    

def __machine_modes_segments(
    action: sim4fjs.instance.MachineAction,
    start: int,
    automated_color
    ):
    """A list containing a single segment representing the action
    
    ('Segment' refers to the colored segments in a row of a Gantt chart)
    """

    segments = []
    segments.append((start, action.duration, automated_color))  
    return segments


def machine_modes_gantt(
    instance: sim4fjs.instance.Instance,
    zoom: float = 1.0,
    legend: bool = True,
    show: bool = True
    ):
    """Draws a Gantt chart of the modes in the specified instance"""

    max_number_of_modes = max(
        len(operation.alternative_actions)
        for operation in instance.operations()
    )

    bars = dict()
    color_categories = {
        automated_color: 'Machine Operation',
    }

    for job in instance.jobs:
        for i in range(max_number_of_modes):
            segments = []
            bars[job.id * max_number_of_modes + i] = segments
            # We use a single int as y coordinate to benefit from
            # matplotlib's automatic choice of which numbers to show.
            # We re-format that later in the ytick_formatter).
            start = job.start
            for operation in job.operations:
                modes = operation.alternative_actions
                if len(modes) > i:
                    segments.extend(
                        __machine_modes_segments(
                            modes[i], start, automated_color
                        )
                    )
                start += operation.max_duration


    def mode_ylabel(y: int):
        """Calculate job id and mode id from a single y number.
        
        This is used to attach a label on any arbitrary point on
        the y axis which represents the list of operations grouped
        by there jobs."""

        mode_id = int(y % max_number_of_modes)
        job_id = int((y - mode_id) / max_number_of_modes)
        return f"j={job_id + 1}, q={mode_id + 1}"


    ytick_formatter = matplotlib.ticker.FuncFormatter(
            lambda y, p: mode_ylabel(y)
        )

    time_axis_descriptor = TimeAxisDescriptor(
        instance.time_horizon(), instance
    )

    fig = gantt(bars, color_categories, [], zoom, legend, show,
                 ylabel="Jobs with Modes", ytick_formatter=ytick_formatter,
                 time_axis_descriptor=time_axis_descriptor)
    instance_file = os.path.basename(
        instance.get_value('input_file', 'Example')
    )
    fig.suptitle(f"{instance_file} - Jobs with Modes")
    return fig


def instance_gantt(
    instance: sim4fjs.instance.Instance,
    zoom: float = 1.0,
    legend: bool = True,
    show: bool = True
    ):
    """Draws a high-level Gantt chart of an instance
    
    Modes are not shown, only the jobs with their slack time until
    due date.
    """

    bars = dict()
    color_categories = {
        'grey': 'Shortest Possible Execution Time',    
        'lightgrey': 'Slack Time until Due Date'
    }

    for job in instance.jobs:
        bars[job.id] = [
            (job.start, job.min_duration, 'grey'),
            (job.start + job.min_duration,
             job.due - job.min_duration - job.start,
             'lightgrey')
        ]

    time_axis_descriptor = TimeAxisDescriptor(
        instance.time_horizon(), instance
    )

    ytick_formatter = matplotlib.ticker.FuncFormatter(
        lambda y, p: f"j={int(y) + 1}"
    )

    fig = gantt(bars, color_categories, [], zoom, legend, show, ylabel="Jobs",
                ytick_formatter=ytick_formatter,
                time_axis_descriptor=time_axis_descriptor)
    instance_file = os.path.basename(
        instance.get_value('input_file', 'Example')
    )
    fig.suptitle(f"{instance_file} - Job Concurrency and Due Dates")
    return fig


def machine_simulation_run_gantt(
    run: SimulationRun,
    zoom: float = 1.0,
    legend: bool = True,
    show: bool = True
    ):
    """Draws a Gantt chart of the specified simulation run
    
    Each machine operation is shown with its start and end as
    determined by the simulation run.
    """

    bars, color_categories = machine_simulation_run_data(run)
    return gantt(bars, color_categories, [], zoom, legend, show)


def machine_simulation_run_data(run: SimulationRun):
    """Translates the simulation run data into Gantt bar data"""

    color_categories = {
        automated_color: 'Machine Operation',
    }

    bars = dict()
    job = None
    for operation in run.operations:
        if operation.job != job:
            job = operation.job
            segments = []
            bars[job.id + 1] = segments
        
        op_run = run.operation_runs[operation]
        machine_duration = op_run.machine_stop - op_run.machine_start
        segments.append(
            (op_run.machine_start, machine_duration, automated_color)
        )
    
    return bars, color_categories


def gantt(bars,
          color_categories,
          vlines = [],
          zoom: float = 1.0,
          legend: bool = True,
          show: bool = True,
          ylabel: str = "Jobs",
          ytick_formatter = None,
          time_axis_descriptor: TimeAxisDescriptor = None):
    """Plots a Gantt chart with colored sections per horizontal bar.

    Parameters
    bars : A dictionary from bar name to the list of sections for
        that bar. Each section is a tuple (start, duration, color name).
        For example:
        bars = {
            '1': [(0, 1, 'blue'), (2, 1, 'blue'), (3, 2, 'red')],
            '2': [(0, 3, 'red'),  (3, 1, 'blue')]
        }

    color_categories : A dictionary from color (as used in the section
        tuples) to the name of the category associated with that color
        for the legend.
        
        For example:
        color_categories = {
            'blue': 'name_in_legend_1',
            'red' : 'name_in_legend_2'
        }
    """

    fig,ax = plt.subplots()
    w, h = fig.get_size_inches()
    fig.set_size_inches(w * zoom, h * zoom)
    
    if time_axis_descriptor is None:
        max_bar_duration = max(
            max((tuple[0] + tuple[1] for tuple in tuples), default = 0)
            for tuples in bars.values()
        )
        time_axis_descriptor = TimeAxisDescriptor(max_bar_duration)

    gantt_from_ax(bars, color_categories, ax,
            time_axis_descriptor=time_axis_descriptor, vlines=vlines,
            legend=legend, show=show, ylabel=ylabel,
            ytick_formatter=ytick_formatter)
    
    return fig


def gantt_from_ax(bars,
          color_categories,
          ax: plt.Axes,
          time_axis_descriptor: TimeAxisDescriptor,
          vlines = [],
          legend: bool = True,
          show: bool = True,
          ylabel: str = "Jobs",
          ytick_formatter = None,
          bar_height = 0.8,
          xlabel: str = None,
          tight_layout: bool = False,
          number_of_yticks: int = 20):
    """Plots a Gantt chart in the given axis

    Parameters
    bars : A dictionary from bar name to the list of sections for
        that bar. Each section is a tuple (start, duration, color name).
        For example:
        bars = {
            '1': [(0, 1, 'blue'), (2, 1, 'blue'), (3, 2, 'red')],
            '2': [(0, 3, 'red'),  (3, 1, 'blue')]
        }

    color_categories : A dictionary from color (as used in the section
        tuples) to the name of the category associated with that color
        for the legend.
        
        For example:
        color_categories = {
            'blue': 'name_in_legend_1',
            'red' : 'name_in_legend_2'
        }
    """

    if len(bars) == 0:
        return

    # our labels on the y-axis
    y = list(bars.keys())

    number_of_columns = max(len(tuples) for tuples in bars.values())

    def create_matrix(index, default):
        """Creates a matrix e.g. for starts or durations of segments
        
        We must turn our dictionary into several matrices suitable
        for plotting a Gantt chart using matplotlib's barh method.

        The reason is that our dictionary specifieds the colored
        sections from left to right for each horizontal bar separately.

        The barh method, however, needs a list of starts and a list
        of widths for each *column* over all bars.

        So for the dictionary
        bars = {
            '1': [(0, 1, 'blue'), (2, 1, 'blue'), (3, 2, 'red')],
            '2': [(0, 3, 'red'),  (3, 1, 'blue')]
        }

        we would create 3 matrices:
        [[0, 0], [2, 3], [3, 0]] for the starts of the 3 columns, 2 bars
        [[1, 3], [1, 1], [2, 0]] for the durations
        [['blue', 'red'], ['blue', 'blue'], ['red', 'white']] for colors
        """
        matrix = []
        for c in range(number_of_columns):
            sections = []
            for bar in bars.values():
                if c < len(bar):
                    sections.append(bar[c][index])
                else:
                    sections.append(default)
            matrix.append(sections)

        return matrix

    start = create_matrix(0, 0)
    duration = create_matrix(1, 0)
    colors = create_matrix(2, 'white')
 
    ax.set_xlim(0, time_axis_descriptor.xlim)
    try_set_real_time_x_ticks(ax, time_axis_descriptor)

    ax.set_xlabel(time_axis_descriptor.xlabel if xlabel == None else xlabel)
    ax.set_ylabel(ylabel)
   
    remaining_legend_colors = set(color_categories.keys())

    for c in range(number_of_columns):

        # if we already exhausted all colors, we need to empty the label
        # otherwise there are too many entries in the legend
        first_color = colors[c][0]
        if first_color in remaining_legend_colors:
            remaining_legend_colors.remove(first_color)
            label = color_categories[first_color]
        else:
            label = ''

        # now draw one column of the horizontal bars (one section)
        ax.barh(
            y,
            width=duration[c],
            height=bar_height,
            left=start[c],
            label=label,
            color = colors[c]
        )

    plt.locator_params(axis='y', nbins=len(bars))

    # we want to show 20..30 ticks or less
    if len(y) > number_of_yticks * 1.5:
        step = int(len(y) / number_of_yticks)
        ax.set_yticks(y[0::step])
    else:
        ax.set_yticks(y)
    
    ax.invert_yaxis()

    if ytick_formatter is not None:
        ax.get_yaxis().set_major_formatter(ytick_formatter)

    if legend:
        ax.legend(ncol=2, bbox_to_anchor=(0, 1), loc='lower left',
                  fontsize='small')

    # for vline in vlines:
    for vline in vlines:
        ax.vlines(vline, 0, 1, colors="r")

    if tight_layout:
        plt.tight_layout()

    if show:
        plt.show()
