"""Defines a class 'Instance' for hosting flexible jobshop problem data

In addition to the machines, jobs, operations, and modes from the fjs,
an instance knows about job concurrency and job due dates.

Typically, an instance is created on the basis of a run.yaml file.
"""

import yaml
import os

import math
import logging
from typing import Iterator, TypeVar, Type, Dict, Optional

import sim4fjs.jobshop
from sim4fjs.utils import set_global_random_seed


# for the return type hint of class methods
I = TypeVar('I', bound='Instance') 


def create_yaml_configuration(yaml_config_file: str) -> dict:
    try: 
        with open(yaml_config_file, 'r') as stream:
            configuration = yaml.safe_load(stream)
    except AttributeError:
        raise ConfigurationError(yaml_config_file)

    return configuration


class Instance:
    """An instance represents a jobshop file and additional parameters

    Note that an instance is meant to store all parameters that control
    simulation or any algorithms.
    
    The idea is that every parameter from the yaml configuration file
    is automatically stored in the instance and thus available for
    users of the instance.
    """

    configuration: dict
    job_concurrency: float
    job_duedate_factor: float
    number_of_jobs: int
    number_of_machines: int


    def __init__(self,
                 configuration: dict,
                 jobshop_instance: sim4fjs.jobshop.Instance):

        """Create an instance from a jobshop instance.

        A jobshop instance only represents the content of an fjs file,
        while this instance also contains parameters from the yaml
        configuration file.

        Here, the yaml configuration is represented by the configuration
        dictionary.
        """

        self._verify_configuration(configuration)

        self.configuration = configuration

        # our obs will have more features than jobshop jobs
        self.jobs = [Job(job) for job in jobshop_instance.jobs]

        # we will not change the number of jobs or machines
        self.number_of_jobs = jobshop_instance.number_of_jobs

        # the only resources we currently have are machines
        self.number_of_machines = jobshop_instance.number_of_machines

        self.random_seed = configuration.get('random_seed')
        if self.random_seed is None:
            self.random_seed = 1

        self._initialize_features()


    def reset_random_seed(self):
        """Reset random seed to the value configured for this instance
        
        When inheriting from this class, call this method when you're
        about to start a randomized process that changes this instance.
        You will then be independent from what happened in other
        randomized processes and thus gain repeatability.

        Note that currently this class does not use any random numbers,
        but inheriting classes may well do so.
        """
        set_global_random_seed(self.random_seed)


    def _verify_configuration(self, configuration:dict):
        """Verifies the consistency of the configuration
        
        (Not all parameters or parameter combinations are valid.)
        """

        inconsistent_parameters = []

        for parameter in configuration:
            # We have the feature that all parameters whose name ends
            # with "in_minutes" are automatically interpreted as minutes
            # instead of simulation time units.
            # Here we check that the same parameter is not specified in
            # minutes *and* stu's.
            if configuration.get(parameter + "_in_minutes"):
                inconsistent_parameters.append(parameter)
        if len(inconsistent_parameters) > 0:
            raise ValueError(
                f"You cannot define the same parameter in both minutes and "
                f"simulation time units: {inconsistent_parameters}."
            )
        
        min_per_stu = configuration.get('minutes_per_simulation_time_unit')
        if min_per_stu is not None:
            if 60 / min_per_stu != int(60 / min_per_stu):
                # this condition makes some arithmetics easier
                raise ValueError(
                    "'minutes_per_simulation_time_unit' must divide 60."
                )


    @classmethod
    def from_configuration(
        cls: Type[I],
        configuration: dict) -> I: # 'I' will work for sub classes too
        """Create an instance from a configuration dictionary
        
        This can be used e.g. in batch processing to override parameters
        coming from a yaml file.
        """
        try:
            input_file = configuration['input_file']
        except KeyError:
            raise ValueError("Please specify the 'input_file' parameter.")

        _, ext = os.path.splitext(input_file)

        if ext == ".py":
            # let the python code create our instance
            return cls.from_python(input_file, configuration)
        else:
            # create the jobshop instance from an fjs text file
            jobshop_instance = sim4fjs.jobshop.Instance.from_file(input_file)

            # and create our instance
            return cls(configuration, jobshop_instance)


    @classmethod
    def from_file(
        cls: Type[I],
        yaml_config_file: str,
        fallbacks: dict = None
        ) -> I: # 'I' will work for sub classes too
        
        """Create an instance from a yaml configuration file
        
        The yaml file must specifiy (among other parameters) a
        jobshop file via the 'input_file' parameter.

        The jobshop file can either be an fjs text file or a .py python
        file that defines a function named like this:
        get_instance(cls, configuration: dict) -> Instance
        """

        yaml_config = create_yaml_configuration(yaml_config_file)

        if fallbacks is not None:
            # merge dictionaries
            yaml_config = {
                **fallbacks,
                **yaml_config
            }

        input_file = yaml_config.get('input_file')
        yaml_config_directory = absolute_directory(yaml_config_file)
        abs_input_file = yaml_config_directory + '/' + input_file

        _, ext = os.path.splitext(abs_input_file)

        if ext == ".py":
            # let the python code create our instance
            return cls.from_python(abs_input_file, yaml_config)
        else:
            # create the jobshop instance from an fjs text file
            jobshop_instance = sim4fjs.jobshop.Instance.from_file(
                abs_input_file
            )

            # and create our instance
            return cls(yaml_config, jobshop_instance)


    @classmethod
    def from_python(
        cls: Type[I],
        abs_python_file: str,
        yaml_config: dict
        ) -> I: # The 'I' type will work for sub classes too
        
        """Create an instance from a python file and a configuration

        The python file must contain a function:
        get_instance(cls, configuration: dict) -> Instance
        """

        root, _ = os.path.splitext(abs_python_file)
        module_name = os.path.basename(root)

        from importlib.util import spec_from_file_location
        spec = spec_from_file_location(module_name, abs_python_file)
        from importlib.util import module_from_spec
        module = module_from_spec(spec)
        spec.loader.exec_module(module)

        return module.get_instance(cls, yaml_config)


    def time_horizon(self) -> int:
        """The time horizon in which jobs will likely be carried out
        
        This is just the end of the last job when all goes as planned.
        Even though it is assumed that all goes as planned, the worst
        case is considered, where jobs are performed taking the longest
        modes.
        """
        return max(
            max(job.due, job.start + job.max_duration) for job in self.jobs
        )


    def operations(self) -> Iterator['Operation']:
        """All operations of the instance in the order of jobs.
        """

        for job in self.jobs:
            for operation in job.operations:
                yield operation


    def actions(self) -> Iterator['MachineAction']:
        """The machine actions of the instance
        
        Each mode of an operation gives rise to such an action.
        The order is: job, operation, mode.
        """

        for job in self.jobs:
            for operation in job.operations:
                for action in operation.alternative_actions:
                    yield action


    def has_value(self, parameter: str) -> bool:
        """Does this instance has a value for the given parameter?"""

        return self.configuration.get(parameter) is not None


    def get_value(self, parameter: str, default = None):
        """Get a value for a parameter from the configuration.

        If the parameter is not specified in the configuration of this
        instance and no default is given as a parameter of get_value,
        a ValueError is raised.
        """

        value = self.configuration.get(parameter)
        
        parameter_in_min = parameter + "_in_minutes"
        if self.has_value(parameter_in_min):
            # automatic conversion of _in_minutes parameters
            stu = self.get_value(parameter_in_min) / self._get_min_per_stu()
            stu_int = int(stu)
            if stu != stu_int:
                raise ValueError(
                    f"Please use multiples of 'minutes_per_simulation_"
                    f"times_unit' for '{parameter_in_min}'."
                )
            return stu_int

        if value is None:
            value = default

        if value is None:
            raise ValueError(
                f"Parameter {parameter} not in configuration"
                f"and no default given."
            )

        logging.info('%s: %s', parameter, value)
        return value
    

    def to_string(self):
        """A comprehensive string representation of this instance

        Note that this format is not (yet) intended to be
        machine-readable to reconstruct an instance.

        It is intended to be human-readable.
        """

        return "Jobs\n" + self._jobs_to_string() + "\n"


    def _get_min_per_stu(self):
        """Gets the minutes per simulation time unit
        
        (Just a convenience method for avoiding using 'get_value')
        """
        return self.get_value('minutes_per_simulation_time_unit')


    def _initialize_features(self) -> None:
        """Initialize the features this instance class supports.
        
        Sub-classes can override this method (but should call the
        super-method) to add their own features.

        Our base class adds (in addition to the fsj features) the
        start and due date of jobs.
        """

        self._set_jobs_start_and_due()


    def _set_jobs_start_and_due(self) -> None:
        """Sets start and due date for all jobs"""

        # here we use sensible hard-coded defaults
        self.job_concurrency = self.get_value('job_concurrency', 0)
        self.job_duedate_factor = self.get_value('job_duedate_factor', 1)
        
        # calculate job start relative to the previous job
        for i in range(len(self.jobs)):
            job = self.jobs[i]
            if i == 0:
                job.start = 0
            else:
                previous_job = self.jobs[i-1]
                job.start = self._calculate_job_start(previous_job)

        # calculate job due relative to job start
        for job in self.jobs:
            job.due = self._calculate_job_due(job)


    def _calculate_job_start(self, previous_job: 'Job') -> None:  
        """Calculates the start of a job, given the previous job
        
        The crucial parameter used is 'job_concurrency'.
        """

        return (
            previous_job.start
            + math.ceil(previous_job.min_duration * self.job_concurrency)
        )


    def _calculate_job_due(self, job: 'Job') -> None:
        """Calculates the due date of the given job"""

        return (
            job.start
            + math.ceil(job.min_duration * self.job_duedate_factor)
        )


    def _jobs_to_string(self):
        """Returns a comprehensive string representation of all jobs
        
        Intended to be human-readable.
        """
        return '\n'.join(
            job.to_string()
            for job in self.jobs
        )


class Job:
    """Represents a job of our instance"""

    start: int
    due: int

    id: int
    number_of_operations: int
    operations: 'list[Operation]'
    min_duration: int
    max_duration: int

    def __init__(self, jobshop_job: sim4fjs.jobshop.Job):
        self.id = jobshop_job.id
        self.number_of_operations = jobshop_job.number_of_operations

        # our operations have more features than jobshop operations
        self.operations = [
            Operation(op, self) for op in jobshop_job.operations
        ]

        # using a generator (without []) to avoid memory allocation
        self.min_duration = sum(
            operation.min_duration for operation in self.operations
        )

        self.max_duration = sum(
            operation.max_duration for operation in self.operations
        )


    def to_string(self):
        """Returns a comprehensive string representation of this job
        """

        job_header = f"Job {self.id} start={self.start}, due={self.due}\n"

        operation_texts = '\n'.join(
            operation.to_string()
            for operation in self.operations
        )

        return job_header + operation_texts


class Operation:
    """A sim4fjs operation that extends a standard jobshop operation.

    Every operation has a list of alternative actions
    """

    job: Job
    id: int
    jobshop_operation: sim4fjs.jobshop.Operation
    alternative_actions: 'list[MachineAction]'
    min_duration: int
    max_duration: int

    def __init__(
        self,
        jobshop_operation: sim4fjs.jobshop.Operation,
        job: Job):

        self.job = job
        self.id = jobshop_operation.id

        self.jobshop_operation = jobshop_operation

        alternative_actions = []
        append = alternative_actions.append
        
        for i in range(jobshop_operation.number_of_modes):
            machine_id = jobshop_operation.machine_id(i)
            duration = jobshop_operation.duration(i)
            alternative_action = MachineAction(self, i, machine_id, duration)
            append(alternative_action)

        self.alternative_actions = alternative_actions

        # using a generator (without []) to avoid memory allocation
        self.min_duration = min(action.duration
                                for action in self.alternative_actions)
        self.max_duration = max(action.duration
                                for action in self.alternative_actions)


    # this will likely be called only once so no need to cache anything
    def next(self) -> 'Operation':
        """Returns the next operation to this one
        
        If this is the last operation of the Job, 'None' is returned.
        """

        next_operation_id = self.id + 1
        if (self.job.number_of_operations > next_operation_id):          
            return self.job.operations[next_operation_id]
        else:
            return None


    # used for tests
    def __repr__(self) -> str:
        return "Op {0} {1};{2}".format(
            self.job.id,
            self.id,
            self.jobshop_operation.__modes)


    def to_string(self) -> str:
        """Returns a comprehensive string representation of this op"""

        operation_header = f"  Operation {self.id}:\n"

        mode_actions = '\n'.join(
            "    " + a.to_string()
            for a in self.alternative_actions
        )   

        return operation_header + mode_actions


class MachineAction:
    """A machine action represents a mode of an operation.
    
    It can have several sub-action: one per sub-action type (which is
    an arbitrary string). Other modules (outside sim4fsj) can define
    sub-action types and implement, add, and simulate sub-actions.
    This can be done by inheriting from the SubAction class and by
    overriding methods from the Simulation class in simulation.py.
    
    A machine action has only one resource - the machine.
    It does not manage alternatives i.e. modes
    (that is done by the operation).
    """
    
    operation: Operation
    mode_index: int
    machine_id: int
    duration: int
    sub_actions: Dict[str, 'SubAction']

    def __init__(self,
                 operation: Operation,
                 mode_index: int,
                 machine_id: int,
                 duration: int):

        self.operation = operation
        self.mode_index = mode_index
        self.machine_id = machine_id
        self.duration = duration
        self.sub_actions = dict()


    def add_or_replace_sub_action(
        self, sub_action_type: str, sub_action: 'SubAction'
        ) -> None:
        """Adds a sub-action of the specified type
    
        Note that if there is already a sub-action of this type,
        that sub-action will be replaced by the specified sub-action.
        """

        self.sub_actions[sub_action_type] = sub_action


    def get_sub_action(
        self, sub_action_type: str
        ) -> Optional['SubAction']:
        """Returns the sub-action with the specified type,
        
        None if there is no such sub action.
        """
        
        return self.sub_actions.get(sub_action_type)


    def to_string(self) -> str:
        """A comprehensive string representation of this action"""

        machine_text = (
            f"machine={self.machine_id:3d},"
            f"duration={self.duration:3d}"
        )

        sub_actions_text = '; '.join(
            self.sub_actions[key].to_string()
            for key in self.sub_actions
        )

        return machine_text + sub_actions_text


class SubAction:
    """A sub action can extend a machine action.

    Inherit from "SubAction" to implement an arbitrary custom action.

    Note that Sim4FJS does currently not implement sub-actions.
    
    Sub-action implementations could be used, e.g., to model setup
    operations, process-related waiting times etc.
    """

    def __init__(self, parent: MachineAction):
        self.parent = parent


    def to_string(self) -> str:
        """ A comprehensive string representation of a sub action       
        """
        return "undefined sub-action"


class ConfigurationError(Exception):
    """Indicates that a yaml configuration file contains a format error.
    
    """
    def __init__(self, config_file: str):
        self.message = "Incorrect format: {0}".format(config_file)
        super().__init__(self.message)


def absolute_directory(config_file: str) -> str:
    """Get the absolute config file directory from the path where python
    was started, see https://note.nkmk.me/en/python-script-file-path/
    """

    config_file_path = os.getcwd() + '/' + config_file
    return os.path.dirname(config_file_path)
