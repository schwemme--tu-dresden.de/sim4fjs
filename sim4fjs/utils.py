"""A collection of generic miscellaneous utility methods."""

import random


global_random_seed = None
def set_global_random_seed(seed: int):
    """Sets the globally used random seed and remembers it.

    This should be the only place where a random seed is actually set
    so tests can always verify it."""

    global global_random_seed
    global_random_seed = seed
    random.seed(seed)


def get_global_random_seed():
    """Get the random seed that is globally used."""
    return global_random_seed


def indexes(list, sub_list):
    """
    The indexes of all objects in the list that are in the sub-list.
    The indexes of all objects in the list that are not in the sub-list.
    
    Assumptions:
    The sub-list contains only objects from the list.
    Objects occur in the same order in the list and the sub-list.
    There are no duplicates in the list or in the sub-list.
    """

    indexes = []
    complement_indexes = []
    
    sub_list_length = len(sub_list)
    
    index = 0
    sub_list_index = 0

    for item in list:
        if (sub_list_index == sub_list_length):
            complement_indexes.append(index)
        else:
            if item == sub_list[sub_list_index]:
                indexes.append(index)
                sub_list_index += 1
            else:
                complement_indexes.append(index)
        index += 1
        
    return indexes, complement_indexes

