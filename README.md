# Sim4FJS - Simulation for Flexible Jobshop

Sim4FJS is a compact tool for running simulations of flexible jobshop instances. It is based on the [SimPy](https://simpy.readthedocs.io/en/latest/) discrete event simulation framework (version 4.0.1) and written in Python.

A flexible jobshop instance contains a list of jobs which are to be carried out on a set of machines. A job is composed of operations, each of which supports several alternative modes of being carried out. For each mode, a specific machine is required.

Instances are specified using a widely used flexible jobshop file format, see below.

Sim4FJS will read an fjs file and simulate the parallel execution of all jobs. Machine allocation and sequencing is based on priority rules: During simulation, for each operation the mode with the shortest duration is automatically selected among the modes whose machine is currently available. If no machine is available, the job queues virtually at all possible machines and it will be assigned to the first machine that becomes available. Within the queue, FIFO (= first in, first out) applies to the sequencing. Only one operation may be carried out on a single machine at a time.

Note that the simulation will simply apply these priority rules without any attempt to optimize the order of operations or assignment to machines in any other way.

As output of the result of the simulation, Sim4FJS creates a csv file specifying for each operation the simulated start and end time and the machine (mode) that was used during simulation.

Sim4FJS provides various Gantt-style diagrams to visualize instances and simulation results.


## Configuring the Simulation
Sim4FJS allows to specify the degree of concurrency for job execution as well as a due date for each job. See  ```example/example.yaml``` for instructions.


## Installation
Make sure you have installed python at least in version 3.7 and the following
python packages, e.g., using pip:

```
pip install matplotlib
pip install pyyaml
pip install simpy
```

Download the sources and try out the ```example/example.ipynb``` Jupyter notebook.

(You cannot install Sim4FJS as a python package yet.)


## Example
In the example directory you will find a Jupyter notebook ```example.ipynb``` showing a exemplary simulation instance.


## Flexible Jobshop Input File Format FJS
Quoting from
https://people.idsia.ch//~monaldo/fjspresults/TextData.zip :

>- In the first line there are (at least) 2 numbers: the first is the number of jobs and the second the number of machines (the 3rd is not necessary, it is the average number of machines per operation)
>- Every row represents one job: the first number is the number of operations of that job, the second number (let's say k>=1) is the number of machines that can process the first operation; then according to k, there are k pairs of numbers (machine, processing time) that specify which are the machines and the processing times; then the data for the second operation and so on...

### Example

```
2   3   1.33
1   1   1   2
2   2   1   3   2   5   1   3   4 
```

- First row:
  - 2 jobs and 3 machines
  - the 3rd number is optional and gives the average number of modes per operation (in our example there are 3 operations with overall 4 modes)
- Second row:
  - the first job has 1 operation which has 1 mode, i.e. can run on one machine, namely on machine 1 with processing time 2
- Third row:
  - the second job has 2 operations:
    - the first operation has 2 modes:
      - machine 1 with processing time 3
      - machine 2 with processing time 5
    - the second operation has 1 mode:
      - machine 3 with processing time 4

### FJS Benchmark Data
Various fjs benchmark data are available, e.g.:

Behnke D, Geiger MJ (2012) Test Instances for the Flexible Job Shop Scheduling Problem with Work Centers. https://doi.org/10.24405/436


## Authors
Sim4FJS was originally created by Julia Schwemmer.
Christian Günsel also contributed to the code.


## Acknowledgment
We would especially like to thank the German Research Foundation / Deutsche Forschungsgemeinschaft (DFG), which is funding our project with the title “A simulation-based and flexi-time applying prediction model for scheduling personnel deployment times in the production planning process of cyber-physical systems” (Project-ID: 439188616; https://gepris.dfg.de/gepris/projekt/439188616).


## License
Sim4FJS is released under the MIT License.
